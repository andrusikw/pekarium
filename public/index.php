<?php

use App\Events;
use App\StackBuilder\StackBuilder;
use App\EventDispatcher;
use App\Subscriber\BalanceUpdateSubscriber;
use App\Subscriber\TransactionSubscriber;

require __DIR__ . '/../vendor/autoload.php';

$stackBuilder = new StackBuilder();
$dispatcher = new EventDispatcher($stackBuilder);
$balanceUpdateSubscriber = new BalanceUpdateSubscriber();
$transactionSubscriber = new TransactionSubscriber();
$transactionSubscriber->setDispatcher($dispatcher);


$dispatcher->subscribe(Events::CREDIT_BALANCE, $transactionSubscriber);
$dispatcher->subscribe(Events::NOTIFY_USER, $balanceUpdateSubscriber);
$dispatcher->subscribe(Events::CALCULATE_BONUS, $transactionSubscriber);
$dispatcher->subscribe(Events::SEND_EMAIL, $transactionSubscriber);


$dispatcher->dispatch(Events::CREDIT_BALANCE);
$dispatcher->dispatch(Events::CALCULATE_BONUS);
$stack = $dispatcher->dispatch(Events::SEND_EMAIL);

dd($stack->build());
