<?php

namespace App;

use App\StackBuilder\StackBuilderInterface;

/**
 * Class EventDispatcher
 * @package App
 */
class EventDispatcher implements EventDispatcherInterface
{
    /**
     * @var array $subscribers
     */
    private array $subscribers = [];

    /**
     * @var StackBuilderInterface $stack
     */
    private StackBuilderInterface $stack;

    /**
     * EventDispatcher constructor.
     * @param StackBuilderInterface $stack
     */
    public function __construct(StackBuilderInterface $stack)
    {
        $this->stack = $stack;
    }

    /**
     * @param string $event
     * @param EventSubscriberInterface $subscriber
     */
    public function subscribe(string $event, EventSubscriberInterface $subscriber): void
    {
        if (isset($subscriber::getSubscribedEvents()[$event])) {
            $method = $subscriber::getSubscribedEvents()[$event];
            $this->subscribers[$event][] = ['object' => $subscriber, 'method' => $method];
        }
    }

    /**
     * @param string $event
     * @return StackBuilderInterface
     */
    public function dispatch($event): StackBuilderInterface
    {
        foreach ($this->subscribers[$event] as $subscriber) {
            $this->stack->addTrace($subscriber, debug_backtrace());
            call_user_func_array([$subscriber['object'], $subscriber['method']], [$event]);
        }

        return $this->stack;
    }
}
