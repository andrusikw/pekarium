<?php

namespace App;

use App\StackBuilder\StackBuilderInterface;

/**
 * Interface EventDispatcherInterface
 * @package App
 */
interface EventDispatcherInterface
{
    /**
     * @param string $event
     * @param EventSubscriberInterface $subscriber
     */
    public function subscribe(string $event, EventSubscriberInterface $subscriber): void;

    /**
     * @param string $event
     * @return StackBuilderInterface
     */
    public function dispatch(string $event): StackBuilderInterface;
}
