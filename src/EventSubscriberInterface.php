<?php

namespace App;

/**
 * Interface EventSubscriberInterface
 * @package App
 */
interface EventSubscriberInterface
{
    /**
     * @return array
     */
    public static function getSubscribedEvents(): array;
}
