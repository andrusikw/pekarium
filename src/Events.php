<?php

namespace App;

/**
 * Class Events
 * @package App
 */
class Events
{
    public const CREDIT_BALANCE = 'credit.balance';
    public const NOTIFY_USER = 'notify.user';
    public const CALCULATE_BONUS = 'calculate.bonuses';
    public const SEND_EMAIL = 'send.email';
}
