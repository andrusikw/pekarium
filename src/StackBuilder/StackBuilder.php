<?php

namespace App\StackBuilder;

/**
 * Class StackBuilder
 * @package App\StackBuilder
 */
class StackBuilder implements StackBuilderInterface
{
    /**
     * @var array $stack
     */
    private array $stack = [];

    /**
     * @var int $stackId
     */
    private static int $stackId = 1;

    /**
     * @param array $subscriber
     * @param array $trace
     * @return $this
     */
    public function addTrace(array $subscriber, array $trace): self
    {
        $id = self::$stackId++;
        array_push($this->stack, [
            'subscriber' => get_class($subscriber['object']),
            'method' => $subscriber['method'],
            'id' => $id,
            'parent_id' => count($trace) > 1 ? $id - 1 : 0,
        ]);

        return $this;
    }

    /**
     * @return array
     */
    public function build(): array
    {
        return $this->buildTree($this->stack);
    }

    /**
     * @param array $elements
     * @param int $parentId
     * @return array
     */
    private function buildTree(array &$elements, $parentId = 0): array
    {
        $branch = array();
        foreach ($elements as $key => $element) {
            if ($element['parent_id'] == $parentId) {
                $children = $this->buildTree($elements, $element['id']);

                if ($children) {
                    $element['children'] = $children;
                }
                $branch[$element['id']] = $element;
                unset($elements[$element['id']]);
            }
        }

        return $branch;
    }
}
