<?php

namespace App\StackBuilder;

/**
 * Interface StackBuilderInterface
 * @package App\StackBuilder
 */
interface StackBuilderInterface
{
    /**
     * @return array
     */
    public function build(): array;
}
