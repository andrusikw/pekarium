<?php

namespace App\Subscriber;

use App\Events;
use App\EventSubscriberInterface;

/**
 * Class BalanceUpdateSubscriber
 * @package App\Subscriber
 */
class BalanceUpdateSubscriber implements EventSubscriberInterface
{

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            Events::NOTIFY_USER => 'onNotifyUser'
        ];
    }

    /**
     * Notify user handle
     */
    public function onNotifyUser()
    {
    }
}
