<?php

namespace App\Subscriber;

use App\Events;
use App\EventDispatcherInterface;
use App\EventSubscriberInterface;

/**
 * Class TransactionSubscriber
 * @package App\Subscriber
 */
class TransactionSubscriber implements EventSubscriberInterface
{
    /**
     * @var EventDispatcherInterface $dispatcher
     */
    private EventDispatcherInterface $dispatcher;

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function setDispatcher(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }


    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            Events::CREDIT_BALANCE => 'onCreditBalance',
            Events::CALCULATE_BONUS => 'onCalculateBonuses',
            Events::SEND_EMAIL => 'onSendMail',
        ];
    }

    /**
     * Credit Balance handle
     */
    public function onCreditBalance()
    {
        $this->dispatcher->dispatch(Events::NOTIFY_USER);
    }

    /**
     * Calculate Bonuses handle
     */
    public function onCalculateBonuses()
    {
        $this->dispatcher->dispatch(Events::CREDIT_BALANCE);
    }

    /**
     * Send mail handle
     */
    public function onSendMail()
    {
    }
}
